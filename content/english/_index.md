---
# Banner
banner:
  title: "CERN (fit &) Boxing Club"
  content: "Welcome to the home page of the CERN fit-boxing club. The boxing club started activities in 2008: a few friends, working at CERN, with non-professional boxing background started gathering in order to stay relatively fit. The club became part of the CERN fitness club, even if it maintained, within the years, a certain level of independence. In its 16 years of operation, the club has seen, I believe, hundreds of people give it a try. The club's ethos has always been to train hard, joke around, and have fun. Several trainers, all volunteering their time, have led the sessions over the years. However, as is common with CERN and the transient nature of the Geneva area, most have eventually moved on—leaving CERN, leaving Geneva, or pursuing other interests. Federico has been a trainer since 2009 and led the majority of the training sessions during this time, while Jacopo is one of the founders. Now (in 2024), we are also moving on. So, after many years, the club ceases operations as they used to be. The fit-boxing activities will likely be incorporated into the offering of the CERN fitness club, though the exact details are still being worked out. We'll keep you updated as things develop. The decision to close this chapter hasn’t been easy. For a few of us, myself included, the club has been a regular and meaningful part of our weekly routine. I’ve made some of my closest friends here. We didn’t just train—we also shared dinners and hosted parties. The impact of COVID changed the club's rhythm, and since then, things haven’t quite been the same. Or perhaps we've simply grown older."
  image: "/images/logo-banner.jpg"
  button:
    enable: false
    label: "Get Started For Free"
    link: "https://github.com/zeon-studio/hugoplate"

# Features
features:
  - title: "Will it resurface?"
    image: "/images/logo-small.jpg"
    content: "Maybe"
    bulletpoints:
      - "Maybe as part of the fitness club"
    button:
      enable: false
      label: "Get Started Now"
      link: "#"

---
